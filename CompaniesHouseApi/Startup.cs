using AutoMapper;
using CompaniesHouseApi.Authorization;
using CompaniesHouseApi.Data;
using CompaniesHouseApi.HealthChecks;
using CompaniesHouseApi.Helpers;
using CompaniesHouseApi.Services.CompaniesHouse;
using CompaniesHouseApi.Services.Governmentservice;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Identity.Web;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using MongoDB.Driver;
using Polly;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CompaniesHouseApi
{
    public class Startup
    {
        public string message = "";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // CORS
            services.AddCors(options =>
            {
                options.AddPolicy(name: MyAllowSpecificOrigins,
                                  builder =>
                                  {
                                      builder.AllowAnyOrigin()
                                                        .AllowAnyHeader()
                                                        .AllowAnyMethod();
                                      
                                  });
            });

            // External Api  Config
            services.Configure<CompaniesHouseServiceSettings>(Configuration.GetSection("CompaniesHouseServiceSettings"));
            services.Configure<GovernmentServiceSettings>(Configuration.GetSection("GovernmentServiceSettings"));

            // Databse Setup
            IMongoDBConfiguration mongoDBConfiguration = Configuration.GetSection("MongoDBConfiguration").Get<MongoDBConfiguration>();

            var mongoClient = new MongoClient(mongoDBConfiguration.ConnectionString);

            services.AddSingleton<IMongoClient>(s => mongoClient);

            var database = mongoClient.GetDatabase(mongoDBConfiguration.DatabaseName);

            services.AddSingleton<IMongoDatabase>(s => database);

            //services.AddSingleton<IMongoDatabase>(s => mongoClient.GetDatabase(mongoDBConfiguration.DatabaseName));

            //services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            //     .AddMicrosoftIdentityWebApi(Configuration.GetSection("AzureAdB2C"));

            // Authentication
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddMicrosoftIdentityWebApi(options =>
            {
                options.MetadataAddress = Configuration["MetadataAddress"];
                options.Audience = Configuration["TokenAudience"];
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateAudience = true,
                    ValidateIssuer = false
                };
                Configuration.Bind("AzureAdB2C", options);
                options.TokenValidationParameters.NameClaimType = "name";
                options.Events = new JwtBearerEvents();
                options.Events.OnTokenValidated =
                   new CustomRoleHelper(options.Events.OnTokenValidated, database).OnTokenValidated;

                options.Events.OnAuthenticationFailed = ctx =>
                {
                    ctx.Response.StatusCode = StatusCodes.Status401Unauthorized;
                    message += "From OnAuthenticationFailed:\n";
                    message += FlattenException(ctx.Exception);
                    return Task.CompletedTask;
                };

                options.Events.OnChallenge = ctx =>
            {
                message += "From OnChallenge:\n";
                ctx.Response.StatusCode = StatusCodes.Status401Unauthorized;
                ctx.Response.ContentType = "text/plain";
                return ctx.Response.WriteAsync(message);
            };

                options.Events.OnMessageReceived = ctx =>
            {
                message = "From OnMessageReceived:\n";
                ctx.Request.Headers.TryGetValue("Authorization", out var BearerToken);
                if (BearerToken.Count == 0)
                    BearerToken = "no Bearer token sent\n";
                message += "Authorization Header sent: " + BearerToken + "\n";
                return Task.CompletedTask;
            };
            },
            options => { Configuration.Bind("AzureAdB2C", options); });

            //// Log and Telemetry
            //services.AddApplicationInsightsTelemetry(Configuration["APPINSIGHTS_CONNECTIONSTRING"]);

            // Scopes
            services.AddAuthorization(options =>
            {
                options.AddPolicy("AccessAsUser",
                    policy => policy.Requirements.Add(new ScopesRequirement("access_as_user")));
            });

            // Api
            services.AddControllers();

            // Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "CompaniesHouse Api",
                    Description = "A simple API to access CompaniesHouse.",
                    Contact = new OpenApiContact
                    {
                        Name = "Orcalo Ltd",
                        Email = "admin@orcalo.co.uk",
                        Url = new Uri("https://orcalo.co.uk")
                    }
                });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description =
         "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                            },
                            Scheme = "oauth2",
                         Name = "Bearer",
                            In = ParameterLocation.Header,
                        }, new List<string>()
                    }
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            // Automapper
            services.AddAutoMapper(typeof(Startup));

            // IOptions
            services.AddOptions();

            // External Api with fault tolerance and retry
            services.AddHttpClient<CompaniesHouseClient>()
                .AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(10, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt))))
                .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(3, TimeSpan.FromSeconds(15)));

            // External Api with fault tolerance and retry
            services.AddHttpClient<TeacherTrainingClient>()
                .AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(10, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt))))
                .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(3, TimeSpan.FromSeconds(15)));

            // External Api with fault tolerance and retry
            services.AddHttpClient<GovernmentServices>()
                .AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(10, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt))))
                .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(3, TimeSpan.FromSeconds(15)));

            // External Api with fault tolerance and retry
            services.AddHttpClient<WorldLocation>()
                .AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(10, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt))))
                .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(3, TimeSpan.FromSeconds(15)));

            // Health
            services.AddHealthChecks()
                .AddCheck<ExternalEndpointsHealthCheck>("ExternalEndpoints");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

            }

            // Handy for debugging - DISABLE IN PRODUCTION
            IdentityModelEventSource.ShowPII = true;

            app.UseCors(MyAllowSpecificOrigins);

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseStaticFiles();

            //app.UseSwaggerAuthorized();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.DocumentTitle = "Orcalo Ltd Api";
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "CompaniesHouse Api v1");
                //c.RoutePrefix = string.Empty;
                c.InjectStylesheet("/swagger-ui/custom.css");
            });

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health");
            });
        }
        public static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();
            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);
                exception = exception.InnerException;
            }
            return stringBuilder.ToString();
        }
    }
}
