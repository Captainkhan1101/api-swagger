﻿using Microsoft.Extensions.Options;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace CompaniesHouseApi.Services.CompaniesHouse
{
    public class CompaniesHouseClient
    {
        private readonly HttpClient _httpClient;

        private readonly CompaniesHouseServiceSettings _companiesHouseServiceSettings;

        public CompaniesHouseClient(HttpClient httpClient, IOptions<CompaniesHouseServiceSettings> options)
        {
            _companiesHouseServiceSettings = options.Value;

            var byteArray = Encoding.ASCII.GetBytes($"{_companiesHouseServiceSettings.ApiKey}:");
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            //httpClient.DefaultRequestHeaders.Add("-u", _companiesHouseServiceSettings.ApiKey);
            _httpClient = httpClient;
        }

        public record Accounts(bool overdue);

        public record Company(string company_name, string status, Accounts accounts);

        public class CompanyResponse
        {
            public Company Company { get; set; }

            public HttpStatusCode StatusCode { get; set; }

            public string Response { get; set; }
        }

        public async Task<CompanyResponse> GetCompanyDetails(string id)
        {
            CompanyResponse companyResponse = new();

            var response = await _httpClient.GetAsync($"https://{_companiesHouseServiceSettings.Host}/company/{id}");

            companyResponse.StatusCode = response.StatusCode;

            if (response.IsSuccessStatusCode)
            {
                companyResponse.Company = await response.Content.ReadFromJsonAsync<Company>();
            }
            else
            {
                companyResponse.Response = await response.Content.ReadAsStringAsync();
            }

            //var company = await _httpClient.GetFromJsonAsync<Company>($"https://{_companiesHouseServiceSettings.Host}/company/{id}");

            return companyResponse;
        }
    }
}
