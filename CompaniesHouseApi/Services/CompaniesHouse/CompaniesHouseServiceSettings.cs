﻿namespace CompaniesHouseApi.Services.CompaniesHouse
{
    public class CompaniesHouseServiceSettings
    {
        public string Host { get; set; }

        public string ApiKey { get; set; }
    }
}
