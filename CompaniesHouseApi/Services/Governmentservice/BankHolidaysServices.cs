﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace CompaniesHouseApi.Services.Governmentservice

{
    public class BankHolidaysServices
    {
        private readonly HttpClient _httpClient;

        private readonly GovernmentServiceSettings _GovernmentServiceSettings;

        public BankHolidaysServices(HttpClient httpClient, IOptions<GovernmentServiceSettings> options)
        {
            _GovernmentServiceSettings = options.Value;

            var byteArray = Encoding.ASCII.GetBytes($"{_GovernmentServiceSettings.ApiKey}:");
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            //httpClient.DefaultRequestHeaders.Add("-u", _companiesHouseServiceSettings.ApiKey);
            _httpClient = httpClient;
        }

        public class events
        {
            public string title { get; set; }
            public DateTime date { get; set; }
            public string notes { get; set; }
            public bool bunting { get; set; }
        }

        public class BankHolidays
        {

            //public string division { get; set; }
            public string title { get; set; }
            //public List<events> events { get; set; }

        };



        public class CompanyResponse
        {
            public BankHolidays BankHolidays { get; set; }

            public HttpStatusCode StatusCode { get; set; }

            public string Response { get; set; }
        }

        public async Task<CompanyResponse> GetCompanyDetails(string id)
        {
            CompanyResponse companyResponse = new();

            var response = await _httpClient.GetAsync($"https://www.gov.uk/api/world-locations/{id}");

            companyResponse.StatusCode = response.StatusCode;

            if (response.IsSuccessStatusCode)
            {
                companyResponse.BankHolidays = await response.Content.ReadFromJsonAsync<BankHolidays>();
            }
            else
            {
                companyResponse.Response = await response.Content.ReadAsStringAsync();
            }

            //var company = await _httpClient.GetFromJsonAsync<Company>($"https://{_companiesHouseServiceSettings.Host}/company/{id}");

            return companyResponse;
        }
    }
}
