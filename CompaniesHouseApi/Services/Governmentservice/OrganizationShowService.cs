﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace CompaniesHouseApi.Services.Governmentservice

{
    public class OrganizationShowService
    {
        private readonly HttpClient _httpClient;

        private readonly GovernmentServiceSettings _GovernmentServiceSettings;

        public OrganizationShowService(HttpClient httpClient, IOptions<GovernmentServiceSettings> options)
        {
            _GovernmentServiceSettings = options.Value;

            var byteArray = Encoding.ASCII.GetBytes($"{_GovernmentServiceSettings.ApiKey}:");
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            //httpClient.DefaultRequestHeaders.Add("-u", _companiesHouseServiceSettings.ApiKey);
            _httpClient = httpClient;
        }






        public class Result
        {
            public int package_count { get; set; }
            public int num_followers { get; set; }
            public string id { get; set; }
            public string category { get; set; }
            public string display_name { get; set; }
            public string approval_status { get; set; }
            public string title { get; set; }
            public string state { get; set; }
            public string description { get; set; }
            public string name { get; set; }
            public DateTime created { get; set; }
            public string type { get; set; }
            public string image_display_url { get; set; }
            public bool is_organization { get; set; }
            public string image_url { get; set; }
            public string revision_id { get; set; }

        }
        public class Publishers
        {
            public bool success { get; set; }
            public Result result { get; set; }
            //public List<events> events { get; set; }

        };



        public class CompanyResponse
        {
            public Publishers Publishers { get; set; }

            public HttpStatusCode StatusCode { get; set; }

            public string Response { get; set; }
        }

        public async Task<CompanyResponse> GetCompanyDetails(string name)
        {
            CompanyResponse companyResponse = new();

            var response = await _httpClient.GetAsync($"https://data.gov.uk/api/action/organization_show?id={name}&include_datasets=false");

            companyResponse.StatusCode = response.StatusCode;

            if (response.IsSuccessStatusCode)
            {
                companyResponse.Publishers = await response.Content.ReadFromJsonAsync<Publishers>();
            }
            else
            {
                companyResponse.Response = await response.Content.ReadAsStringAsync();
            }

            //var company = await _httpClient.GetFromJsonAsync<Company>($"https://{_companiesHouseServiceSettings.Host}/company/{id}");

            return companyResponse;
        }
    }
}
