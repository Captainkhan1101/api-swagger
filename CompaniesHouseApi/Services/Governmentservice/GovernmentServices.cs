﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace CompaniesHouseApi.Services.Governmentservice

{
    public class GovernmentServices
    {
        private readonly HttpClient _httpClient;

        private readonly GovernmentServiceSettings _companiesHouseServiceSettings;

        public GovernmentServices(HttpClient httpClient, IOptions<GovernmentServiceSettings> options)
        {
            _companiesHouseServiceSettings = options.Value;

            var byteArray = Encoding.ASCII.GetBytes($"{_companiesHouseServiceSettings.ApiKey}:");
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            //httpClient.DefaultRequestHeaders.Add("-u", _companiesHouseServiceSettings.ApiKey);
            _httpClient = httpClient;
        }

      
        public class Details
        {
            public DateTime? start_date { get; set; }
            public DateTime? end_date { get; set; }
        }

        public class links
        {
            public string href { get; set; }
            public string rel { get; set; }
        }

        public class ResponsiveInfo
        {
            public string status { get; set; }
            public List<links> links   { get; set; }
        }


        public class GovernmentDetails {
            public string title { get; set; }
            public string slug { get; set; }
            public Details details { get; set; }
            public ResponsiveInfo _response_info { get; set; }

        };

        public class CompanyResponse
        {
            public GovernmentDetails GovernmentDetails { get; set; }

            public HttpStatusCode StatusCode { get; set; }

            public string Response { get; set; }
        }

        public async Task<CompanyResponse> GetCompanyDetails(string name)
        {
            CompanyResponse companyResponse = new();

            var response = await _httpClient.GetAsync($"https://www.gov.uk/api/governments/{name}");

            companyResponse.StatusCode = response.StatusCode;

            if (response.IsSuccessStatusCode)
            {
                companyResponse.GovernmentDetails = await response.Content.ReadFromJsonAsync<GovernmentDetails>();
            }
            else
            {
                companyResponse.Response = await response.Content.ReadAsStringAsync();
            }

            //var company = await _httpClient.GetFromJsonAsync<Company>($"https://{_companiesHouseServiceSettings.Host}/company/{id}");

            return companyResponse;
        }
    }
}
