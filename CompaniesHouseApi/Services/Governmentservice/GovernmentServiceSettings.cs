﻿namespace CompaniesHouseApi.Services.Governmentservice
{
    public class GovernmentServiceSettings
    {
        public string Host { get; set; }

        public string ApiKey { get; set; }
    }
}
