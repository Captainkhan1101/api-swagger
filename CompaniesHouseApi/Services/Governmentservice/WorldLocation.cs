﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace CompaniesHouseApi.Services.Governmentservice

{
    public class WorldLocation
    {
        private readonly HttpClient _httpClient;

        private readonly GovernmentServiceSettings _GovernmentServiceSettings;

        public WorldLocation(HttpClient httpClient, IOptions<GovernmentServiceSettings> options)
        {
            _GovernmentServiceSettings = options.Value;

            var byteArray = Encoding.ASCII.GetBytes($"{_GovernmentServiceSettings.ApiKey}:");
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            //httpClient.DefaultRequestHeaders.Add("-u", _companiesHouseServiceSettings.ApiKey);
            _httpClient = httpClient;
        }

      
        public class details
        {
            public string slug { get; set; }
            public string iso2 { get; set; }
        }

        public class Organisations
        {
            public string id { get; set; }
            public string web_url { get; set; }
        }

        public class links
        {
            public string href { get; set; }
            public string rel { get; set; }
        }

        //public class _response_info
        //{
        //    public string status { get; set; }
        //    public List<links> links   { get; set; }
        //}


        public class CountryDetails {
            public string id { get; set; }
            public string title { get; set; }
            public string formate { get; set; }
            public DateTime? updated_at { get; set; }
            public string web_url { get; set; }
            public string analytics_identifier { get; set; }
            public details details { get; set; }
            public Organisations organisations { get; set; }
            public string content_id { get; set; }
            //public _response_info _response_info { get; set; }

        };

        public class CompanyResponse
        {
            public CountryDetails CountryDetails { get; set; }

            public HttpStatusCode StatusCode { get; set; }

            public string Response { get; set; }
        }

        public async Task<CompanyResponse> GetCompanyDetails(string name)
        {
            CompanyResponse companyResponse = new();

            var response = await _httpClient.GetAsync($"https://www.gov.uk/api/world-locations/{name}");

            companyResponse.StatusCode = response.StatusCode;

            if (response.IsSuccessStatusCode)
            {
                companyResponse.CountryDetails = await response.Content.ReadFromJsonAsync<CountryDetails>();
            }
            else
            {
                companyResponse.Response = await response.Content.ReadAsStringAsync();
            }

            //var company = await _httpClient.GetFromJsonAsync<Company>($"https://{_companiesHouseServiceSettings.Host}/company/{id}");

            return companyResponse;
        }
    }
}
