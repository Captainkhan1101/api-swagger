﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace CompaniesHouseApi.Services.CompaniesHouse
{
    public class TeacherTrainingClient
    {
        private readonly HttpClient _httpClient;

        public TeacherTrainingClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public class Attributes
        {
            public string accredited_body_code { get; set; }
            public int? age_maximum { get; set; }
            public int? age_minimum { get; set; }
            public string bursary_amount { get; set; }
            public List<string> bursary_requirements { get; set; }
            public DateTime created_at { get; set; }
            public string funding_type { get; set; }
            public List<string> gcse_subjects_required { get; set; }
            public string level { get; set; }
            public string name { get; set; }
            public string program_type { get; set; }
            public List<string> qualifications { get; set; }
            public string scholarship_amount { get; set; }
            public string study_mode { get; set; }
            public string uuid { get; set; }
            public string about_accredited_body { get; set; }
            public string applications_open_from { get; set; }
            public DateTime changed_at { get; set; }
            public string code { get; set; }
            public bool findable { get; set; }
            public bool has_early_career_payments { get; set; }
            public bool has_scholarship { get; set; }
            public bool has_vacancies { get; set; }
            public bool is_send { get; set; }
            public DateTime? last_published_at { get; set; }
            public bool open_for_applications { get; set; }
            public string required_qualifications_english { get; set; }
            public string required_qualifications_maths { get; set; }
            public string required_qualifications_science { get; set; }
            public bool running { get; set; }
            public string start_date { get; set; }
            public string state { get; set; }
            public string summary { get; set; }
            public string about_course { get; set; }
            public string course_length { get; set; }
            public string fee_details { get; set; }
            public int? fee_international { get; set; }
            public int? fee_domestic { get; set; }
            public string financial_support { get; set; }
            public string how_school_placements_work { get; set; }
            public string interview_process { get; set; }
            public string other_requirements { get; set; }
            public string personal_qualities { get; set; }
            public string required_qualifications { get; set; }
            public string salary_details { get; set; }
        }

        public class Meta
        {
            public bool included { get; set; }
        }

        public class AccreditedBody
        {
            public Meta meta { get; set; }
        }

        public class Provider
        {
            public Meta meta { get; set; }
        }

        public class RecruitmentCycle
        {
            public Meta meta { get; set; }
        }

        public class Relationships
        {
            public AccreditedBody accredited_body { get; set; }
            public Provider provider { get; set; }
            public RecruitmentCycle recruitment_cycle { get; set; }
        }

        public class Datum
        {
            public string id { get; set; }
            public string type { get; set; }
            public Attributes attributes { get; set; }
            public Relationships relationships { get; set; }
        }

        public class Links
        {
            public string first { get; set; }
            public string last { get; set; }
            public object prev { get; set; }
            public string next { get; set; }
        }

        public class Jsonapi
        {
            public string version { get; set; }
        }

        public class Root
        {
            public List<Datum> data { get; set; }
            public Links links { get; set; }
            public Jsonapi jsonapi { get; set; }
        }

        public class TeacherTrainingResponse
        {
            public Root Root { get; set; }

            public HttpStatusCode StatusCode { get; set; }

            public string Response { get; set; }
        }


        public async Task<TeacherTrainingResponse> GetCourses(string year)
        {
            TeacherTrainingResponse teacherTrainingResponse = new();

            var response = await _httpClient.GetAsync($"https://api.publish-teacher-training-courses.service.gov.uk/api/public/v1/recruitment_cycles/{year}/courses");


            teacherTrainingResponse.StatusCode = response.StatusCode;

            if (response.IsSuccessStatusCode)
            {
                teacherTrainingResponse.Root = await response.Content.ReadFromJsonAsync<Root>();
            }
            else
            {
                teacherTrainingResponse.Response = await response.Content.ReadAsStringAsync();
            }

            //var company = await _httpClient.GetFromJsonAsync<Company>($"https://{_companiesHouseServiceSettings.Host}/company/{id}");

            return teacherTrainingResponse;
        }
    }
}
