using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using System;

namespace CompaniesHouseApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //// Check if app is running on Azure, the WEBSITE_SITE_NAME environment variable will be set if it is.
            //if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("WEBSITE_SITE_NAME")))
            //{
            //    Log.Logger = new LoggerConfiguration()
            //        .MinimumLevel.Information()
            //        .Enrich.WithMachineName()
            //        .Enrich.WithProcessId()
            //        .Enrich.FromLogContext()
            //        .WriteTo.File(path: @"D:\home\LogFiles\Application\log.txt", fileSizeLimitBytes: 1_000_000,
            //            flushToDiskInterval: TimeSpan.FromSeconds(5), shared: true,
            //            restrictedToMinimumLevel: LogEventLevel.Debug)
            //        .CreateLogger();
            //}
            //else
            //{
            //    // Not Azure, just log to Console, no need to persist
            //    Log.Logger = new LoggerConfiguration()
            //        .MinimumLevel.Debug()
            //        .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
            //        .WriteTo.Console()
            //        .CreateLogger();
            //}

            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
               .Enrich.WithMachineName()
               .Enrich.WithProcessId()
               .Enrich.FromLogContext()
               .WriteTo.Console()
               //.WriteTo.ApplicationInsights(telemetryConfiguration,
               // TelemetryConverter.Traces)
               .WriteTo.File("logs/log.txt", rollingInterval: RollingInterval.Day)
               //.WriteTo.File(new RenderedCompactJsonFormatter(), "logs/log.ndjson", rollingInterval: RollingInterval.Day)
               .CreateLogger();

            try
            {
                Log.Information("Starting Api");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Api start-up failed");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
