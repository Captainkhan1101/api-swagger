﻿using AutoMapper;
using CompaniesHouseApi.Services.Governmentservice;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static CompaniesHouseApi.Services.Governmentservice.BankHolidaysServices;

namespace CompaniesHouseApi.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BankHolidaysController : ControllerBase
    {
        private readonly BankHolidaysServices _BankHolidaysServices;
        private readonly IMapper _mapper;

        public BankHolidaysController(IMapper mapper, BankHolidaysServices bankHolidaysServices)
        {
            _BankHolidaysServices = bankHolidaysServices;
            _mapper = mapper;
        }

        ///<summary>
        /// Get Company using Id
        ///</summary>
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<BankHolidays>> GetArea(string id)
        {
            var companyResponse = await _BankHolidaysServices.GetCompanyDetails(id);

            if (companyResponse.BankHolidays == null)
            {
                return NotFound(companyResponse.Response);
            }

            //if(companyResponse.StatusCode == System.Net.HttpStatusCode.)

            //var companiesHouseResource = _mapper.Map<CompaniesHouseModel, CompaniesHouseResource>(companiesHouse);

            return Ok(companyResponse.BankHolidays);
        }
    }
}
