﻿using AutoMapper;
using CompaniesHouseApi.Services.Governmentservice;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static CompaniesHouseApi.Services.Governmentservice.GovernmentServices;

namespace CompaniesHouseApi.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class GovernmentServiceController : ControllerBase
    {
        private readonly GovernmentServices _GovernmentServices;
        private readonly IMapper _mapper;

        public GovernmentServiceController(IMapper mapper, GovernmentServices governmentServices)
        {
            _GovernmentServices = governmentServices;
            _mapper = mapper;
        }

        ///<summary>
        /// Get Company using Id
        ///</summary>
        [HttpGet("[action]/{name}")]
        public async Task<ActionResult<GovernmentDetails>> GetName(string name)
        {
            var companyResponse = await _GovernmentServices.GetCompanyDetails(name);

            if (companyResponse.GovernmentDetails == null)
            {
                return NotFound(companyResponse.Response);
            }

            //if(companyResponse.StatusCode == System.Net.HttpStatusCode.)

            //var companiesHouseResource = _mapper.Map<CompaniesHouseModel, CompaniesHouseResource>(companiesHouse);

            return Ok(companyResponse.GovernmentDetails);
        }
    }
}
