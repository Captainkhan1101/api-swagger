﻿using AutoMapper;
using CompaniesHouseApi.Services.CompaniesHouse;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static CompaniesHouseApi.Services.CompaniesHouse.CompaniesHouseClient;

namespace CompaniesHouseApi.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CompaniesHouseController : ControllerBase
    {
        private readonly CompaniesHouseClient _companiesHouseClient;
        private readonly IMapper _mapper;

        public CompaniesHouseController(IMapper mapper, CompaniesHouseClient companiesHouseClient)
        {
            _companiesHouseClient = companiesHouseClient;
            _mapper = mapper;
        }

        ///<summary>
        /// Get Company using Id
        ///</summary>
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<Company>> GetCompany(string id)
        {
            var companyResponse = await _companiesHouseClient.GetCompanyDetails(id);

            if (companyResponse.Company == null)
            {
                return NotFound(companyResponse.Response);
            }

            //if(companyResponse.StatusCode == System.Net.HttpStatusCode.)

            //var companiesHouseResource = _mapper.Map<CompaniesHouseModel, CompaniesHouseResource>(companiesHouse);

            return Ok(companyResponse.Company);
        }
    }
}
