﻿using AutoMapper;
using CompaniesHouseApi.Services.CompaniesHouse;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static CompaniesHouseApi.Services.CompaniesHouse.CompaniesHouseClient;
using static CompaniesHouseApi.Services.CompaniesHouse.TeacherTrainingClient;

namespace CompaniesHouseApi.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class TeacherTrainingController : ControllerBase
    {
        private readonly TeacherTrainingClient _teacherTrainingClient;
        private readonly IMapper _mapper;

        public TeacherTrainingController(IMapper mapper, TeacherTrainingClient teacherTrainingClient)
        {
            _teacherTrainingClient = teacherTrainingClient;
            _mapper = mapper;
        }

        ///<summary>
        /// Get Courses using year
        ///</summary>
        [HttpGet("[action]/{year}")]
        public async Task<ActionResult<Root>> GetCourses(string year)
        {
            var teacherTrainingResponse = await _teacherTrainingClient.GetCourses(year);

            if (teacherTrainingResponse.Root == null)
            {
                return NotFound(teacherTrainingResponse.Response);
            }

            //if(companyResponse.StatusCode == System.Net.HttpStatusCode.)

            //var companiesHouseResource = _mapper.Map<CompaniesHouseModel, CompaniesHouseResource>(companiesHouse);

            return Ok(teacherTrainingResponse.Root);
        }
    }
}
