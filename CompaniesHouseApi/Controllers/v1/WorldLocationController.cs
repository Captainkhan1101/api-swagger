﻿using AutoMapper;
using CompaniesHouseApi.Services.Governmentservice;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static CompaniesHouseApi.Services.Governmentservice.WorldLocation;

namespace CompaniesHouseApi.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class WorldLocationController : ControllerBase
    {
        private readonly WorldLocation _WorldLocation;
        private readonly IMapper _mapper;

        public WorldLocationController(IMapper mapper, WorldLocation WorldLocation)
        {
            _WorldLocation = WorldLocation;
            _mapper = mapper;
        }

        ///<summary>
        /// Get Company using Id
        ///</summary>
        [HttpGet("[action]/{name}")]
        public async Task<ActionResult<CountryDetails>> GetCountryName(string name)
        {
            var companyResponse = await _WorldLocation.GetCompanyDetails(name);

            if (companyResponse.CountryDetails == null)
            {
                return NotFound(companyResponse.Response);
            }

            //if(companyResponse.StatusCode == System.Net.HttpStatusCode.)

            //var companiesHouseResource = _mapper.Map<CompaniesHouseModel, CompaniesHouseResource>(companiesHouse);

            return Ok(companyResponse.CountryDetails);
        }
    }
}
