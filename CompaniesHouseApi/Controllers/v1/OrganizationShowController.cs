﻿using AutoMapper;
using CompaniesHouseApi.Services.Governmentservice;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static CompaniesHouseApi.Services.Governmentservice.OrganizationShowService;

namespace CompaniesHouseApi.Controllers.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class OrganizationShowController : ControllerBase
    {
        private readonly OrganizationShowService _OrganizationShowService;
        private readonly IMapper _mapper;

        public OrganizationShowController(IMapper mapper, OrganizationShowService OrganizationShowService)
        {
            _OrganizationShowService = OrganizationShowService;
            _mapper = mapper;
        }

        ///<summary>
        /// Get Company using Id
        ///</summary>
        [HttpGet("[action]/{name}")]
        public async Task<ActionResult<Publishers>> GetName(string name)
        {
            var companyResponse = await _OrganizationShowService.GetCompanyDetails(name);

            if (companyResponse.Publishers == null)
            {
                return NotFound(companyResponse.Response);
            }

            //if(companyResponse.StatusCode == System.Net.HttpStatusCode.)

            //var companiesHouseResource = _mapper.Map<CompaniesHouseModel, CompaniesHouseResource>(companiesHouse);

            return Ok(companyResponse.Publishers);
        }
    }
}
