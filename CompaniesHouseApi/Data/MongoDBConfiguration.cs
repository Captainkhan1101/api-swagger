﻿namespace CompaniesHouseApi.Data
{
    public interface IMongoDBConfiguration
    {
        string ConnectionString { get; }

        string DatabaseName { get; }

    }
    public class MongoDBConfiguration : IMongoDBConfiguration
    {
        public string ConnectionString { get; set; }

        public string DatabaseName { get; set; }
    }
}
