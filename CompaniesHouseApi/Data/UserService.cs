﻿
using CompaniesHouseApi.Data.MongoDB;
using CompaniesHouseApi.Models;
using MongoDB.Driver;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompaniesHouseApi.Services.Database
{
    public class UserService : MongoDBService<UserModel>
    {
        private readonly IMongoCollection<UserModel> _col;

        public UserService(IMongoDatabase database) : base(database, "users")
        {
            _col = database.GetCollection<UserModel>("users");
        }

        public async Task<string> CheckId(string id)
        {
            try
            {
                var fieldsBuilder = Builders<UserModel>.Projection;
                var fields = fieldsBuilder.Include(d => d.Id);

                var obj = await _col.Find(x => x.Id == id).Project<UserModel>(fields)
                                .FirstOrDefaultAsync();

                if(obj !=null)
                {
                    return obj.Id;
                }
                else
                {
                    return null;
                }  
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }

        public async Task<Tuple<string, List<string>>> CheckIdEdit(string id)
        {
            try
            {
                var fieldsBuilder = Builders<UserModel>.Projection;
                var fields = fieldsBuilder.Include(d => d.Id).Include(d => d.Roles);

                var obj = await _col.Find(x => x.Id == id).Project<UserModel>(fields)
                                .FirstOrDefaultAsync();

                if (obj != null)
                {
                    return new Tuple<string, List<string>>(obj.Id, obj.Roles);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }

        public async Task<Tuple<string,string>> CheckUser(string email)
        {
            try
            {
                var fieldsBuilder = Builders<UserModel>.Projection;
                var fields = fieldsBuilder.Include(d => d.Email);

                var obj = await _col.Find(x => x.Email == email).Project<UserModel>(fields)
                                .FirstOrDefaultAsync();

                if (obj != null)
                {
                    return new Tuple<string, string>(obj.Email, obj.Id);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }

        public async Task<string> GetIdByEmail(string email)
        {
            try
            {
                var fieldsBuilder = Builders<UserModel>.Projection;
                var fields = fieldsBuilder.Include(d => d.Id);

                var obj = await _col.Find(x => x.Email == email).Project<UserModel>(fields)
                                .FirstOrDefaultAsync();

                return obj?.Id;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }

        public async Task<string> GetEmailById(string id)
        {
            try
            {
                var fieldsBuilder = Builders<UserModel>.Projection;
                var fields = fieldsBuilder.Include(d => d.Email);

                var obj = await _col.Find(x => x.Id == id).Project<UserModel>(fields)
                                .FirstOrDefaultAsync();

                return obj?.Email;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }

        public async Task<List<string>> GetRolesByEmail(string email)
        {
            try
            {
                var fieldsBuilder = Builders<UserModel>.Projection;
                var fields = fieldsBuilder.Include(d => d.Roles);

                var obj = await _col.Find(x => x.Email == email).Project<UserModel>(fields)
                                .FirstOrDefaultAsync();

                return obj?.Roles;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        }
    }
}
