﻿using MongoDB.Bson;
using MongoDB.Driver;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CompaniesHouseApi.Data.MongoDB
{
    public class MongoDBService<T> where T : class
    {
        private readonly IMongoCollection<T> _col;

        public MongoDBService(IMongoDatabase database, string collectionName)
        {
            _col = database.GetCollection<T>(collectionName);
        }

        //public async Task<(IEnumerable<T>, bool)> GetAll()
        //{
        //    try
        //    {
        //        return (await _col.Find(_ => true).ToListAsync(), false);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error("Database Error", ex);
        //        return (new List<T>(), true);
        //    }
        //}

        public async Task<IEnumerable<T>> GetAll()
        {
            try
            {
                return await _col.Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                Log.Error("Database Error {0}", ex);
                return new List<T>();
            }
        }

        public async Task<T> Get(string id)
        {
            try
            {
                return await _col.Find(Builders<T>.Filter.Eq("Id", id))
                                .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                Log.Error("Database Error {0}", ex);
                return null;
            }
        }

        public async Task<T> Add(T item)
        {
            try
            {
                await _col.InsertOneAsync(item);
                return item;
            }
            catch (Exception ex)
            {
                Log.Error("Database Error {0}", ex);
                return null;
            }
        }

        public async Task<bool> Replace(string id, T item)
        {
            try
            {
                ReplaceOneResult actionResult
                    = await _col.ReplaceOneAsync(Builders<T>.Filter.Eq("Id", id)
                                            , item
                                            , new ReplaceOptions { IsUpsert = true });
                return actionResult.IsAcknowledged;
                //&& actionResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                Log.Error("Database Error {0}", ex);
                return false;
            }
        }

        public async Task<bool> Remove(string id)
        {
            try
            {
                DeleteResult actionResult
                    = await _col.DeleteOneAsync(
                        Builders<T>.Filter.Eq("Id", id));

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                Log.Error("Database Error {0}", ex);
                return false;
            }
        }

        public async Task<bool> RemoveAll()
        {
            try
            {
                DeleteResult actionResult
                    = await _col.DeleteManyAsync(new BsonDocument());

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                Log.Error("Database Error {0}", ex);
                return false;
            }
        }
    }
}
