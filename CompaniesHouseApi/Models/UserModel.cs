﻿using CompaniesHouseApi.Data.MongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompaniesHouseApi.Models
{
    public class UserModel : MongoDBEntity
    {
        public string Email { get; set; }

        public List<string> Roles { get; set; }
    }
}
