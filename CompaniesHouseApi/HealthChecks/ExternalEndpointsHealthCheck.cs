﻿using CompaniesHouseApi.Services.CompaniesHouse;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;
using Serilog;
using System;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;

namespace CompaniesHouseApi.HealthChecks
{
    public class ExternalEndpointsHealthCheck : IHealthCheck
    {
        private readonly CompaniesHouseServiceSettings _companiesHouseServiceSettings;

        public ExternalEndpointsHealthCheck(IOptions<CompaniesHouseServiceSettings> options)
        {
            _companiesHouseServiceSettings = options.Value;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                Ping ping = new();

                var reply = await ping.SendPingAsync(_companiesHouseServiceSettings.Host);

                if (reply.Status != IPStatus.Success)
                {
                    return HealthCheckResult.Unhealthy();
                }

                return HealthCheckResult.Healthy();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);

                return HealthCheckResult.Healthy();
            }
        }
    }
}
