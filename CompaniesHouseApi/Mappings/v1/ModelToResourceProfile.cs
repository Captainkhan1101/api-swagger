﻿using AutoMapper;
using CompaniesHouseApi.Models.v1;
using CompaniesHouseApi.Resources.v1;

namespace CompaniesHouseApi.Mapping.v1
{
    public class ModelToResourceProfile : Profile
    {
        public ModelToResourceProfile()
        {
            CreateMap<CompaniesHouseModel, CompaniesHouseResource>();
        }
    }
}
