﻿using CompaniesHouseApi.Models;
using CompaniesHouseApi.Services.Database;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using MongoDB.Driver;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CompaniesHouseApi.Helpers
{
    public class CustomRoleHelper
    {
        private readonly Func<TokenValidatedContext, Task> _onTokenValidated;
        private readonly UserService _userService;

        public CustomRoleHelper(Func<TokenValidatedContext, Task> onTokenValidated, IMongoDatabase database)
        {
            _onTokenValidated = onTokenValidated;
            _userService = new UserService(database);
        }

        public Task OnTokenValidated(TokenValidatedContext context)
        {
            _onTokenValidated?.Invoke(context);
            return Task.Run(async () =>
            {
                try
                {
                    var emailClaim = context.Principal.Claims.FirstOrDefault(c => c.Type == "emails");

                    if (!string.IsNullOrWhiteSpace(emailClaim?.Value))
                    {
                        var userId = await _userService.GetIdByEmail(emailClaim?.Value);

                        if(!string.IsNullOrEmpty(userId))
                        {
                            var groups = await _userService.GetRolesByEmail(emailClaim?.Value);

                            foreach (var group in groups)
                            {
                                ((ClaimsIdentity)context.Principal.Identity).AddClaim(new Claim(ClaimTypes.Role,
                               group, ClaimValueTypes.String));
                            }
                        }
                        else
                        {
                            var user = new UserModel();

                            user.Email = emailClaim?.Value;

                            user.Roles = new List<string>();

                            var done = await _userService.Add(user);
                        }                        
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("Database Error {0}", ex);
                }
            });
        }
    }
}
